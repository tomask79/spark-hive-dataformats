%spark

import org.apache.spark.sql.hive.HiveContext;

var hiveContext = new HiveContext(sc);

// CSV Table
hiveContext.sql(" CREATE EXTERNAL TABLE IF NOT EXISTS CSVMovies (id String, genre String, name String, country String) "+
                " row format delimited "+
                " fields terminated by ','"+
                " STORED AS TEXTFILE "+
                " location '/tests/dir'"
);

// ORC Table
hiveContext.sql("CREATE TABLE IF NOT EXISTS ORCExpMovies(id String, genre String, name String, country String) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ',' "+
                " STORED AS ORC"
);

// CSV -> ORC (most efficient way!)
hiveContext.sql("INSERT OVERWRITE TABLE ORCExpMovies SELECT * from CSVMovies");

// Another empty ORC Movies table
hiveContext.sql(" DROP TABLE IF EXISTS UsaMovies ");
hiveContext.sql(" CREATE TABLE UsaMovies(id String, genre String, name String, country String) "+
                " ROW FORMAT DELIMITED "+
                " FIELDS TERMINATED BY ',' "+
                " STORED AS ORC "
);

// Transformation query -> New Dataframe -> Import to new orc table...
val moviesDF = hiveContext.sql("select * from ORCExpMovies where country ='USA'");
moviesDF.write.mode("overwrite").format("orc").saveAsTable("AuxTable");

// ORC -> ORC
hiveContext.sql(" INSERT OVERWRITE TABLE UsaMovies SELECT * FROM AuxTable ");
val parquetDF = hiveContext.sql(" SELECT * from UsaMovies ");

// ORC DF -> parquet
parquetDF.write.mode("overwrite").format("parquet").save("/tests/parquetTabule");

// parquet -> Dataframe
var parDF = hiveContext.read.format("parquet").load("/tests/parquetTabule");
parDF.show();